import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class BingHomeWorkTest {

    @Test
    public void navigateToBing() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\ytoli\\Driver\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();

        webDriver.get("https://www.bing.com/");

        WebElement inputSearch = webDriver.findElement(By.id("sb_form_q"));
        inputSearch.sendKeys("Telerik Academy Alpha");
        WebElement SearchButton = webDriver.findElement(By.xpath("//label[@for='sb_form_go']"));
        SearchButton.click();

        WebElement resultList = webDriver.findElement(By.id("b_results"));
        List<WebElement> results = resultList.findElements(By.tagName("li"));
        WebElement actualResult = results.get(0).findElement(By.xpath("//h2[@class=' b_topTitle']"));
        String expectedResult = "IT Career Start in 6 Months - Telerik Academy Alpha";

        Assert.assertEquals(expectedResult, actualResult.getText());

        webDriver.quit();
    }
}
