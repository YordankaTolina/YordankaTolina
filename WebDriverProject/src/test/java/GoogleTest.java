import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class GoogleTest {
    @Test
    public void navigateToGoogle() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\ytoli\\Driver\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();

        webDriver.get("https://www.google.com/");

        WebElement consentToCookies = webDriver.findElement(By.xpath("//button[@id='L2AGLb']"));
        consentToCookies.click();

        WebElement inputSearch = webDriver.findElement(By.xpath("//input[@class='gLFyf gsfi']"));
        inputSearch.sendKeys("Telerik Academy Alpha");
        inputSearch.submit();

        WebElement topResult = webDriver.findElement(By.xpath("//h3[@class='LC20lb MBeuO DKV0Md']"));
        String expectedTitle = "IT Career Start in 6 Months - Telerik Academy Alpha";
        String actualTitle = topResult.getText();
        Assert.assertEquals(expectedTitle, actualTitle);
        webDriver.quit();
    }
}




