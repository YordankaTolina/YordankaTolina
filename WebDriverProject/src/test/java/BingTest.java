import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class BingTest {
    @Test
    public void navigateToBing() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\ytoli\\Driver\\chromedriver.exe");

        WebDriver webDriver = new ChromeDriver();
        webDriver.get("https://www.bing.com/");
        WebElement inputSearch = webDriver.findElement(By.id("sb_form_q"));

        Assert.assertTrue("Search input is not displayed.", inputSearch.isDisplayed());
        webDriver.quit();
    }

    @Test
    public void navigateToBingByXpath() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\ytoli\\Driver\\chromedriver.exe");

        WebDriver webDriver = new ChromeDriver();
        webDriver.get("https://www.bing.com/");
        WebElement inputSearch = webDriver.findElement(By.xpath("//input[@id='sb_form_q']"));
        Assert.assertTrue("Search input is not displayed.", inputSearch.isDisplayed());
        webDriver.quit();
    }
}

